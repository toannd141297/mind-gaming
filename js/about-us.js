$(".bg-menu").click(function () {
  if ($(this).hasClass("active")) {
    $(this).removeClass("active");
    $("header .list-page").removeClass("active");
  } else {
    $(this).addClass("active");
    $("header .list-page").addClass("active");
  }
});

$(".language").click(function () {
  if ($(".language .list-lang").hasClass("active")) {
    $(".language .list-lang").removeClass("active");
  } else {
    $(".language .list-lang").addClass("active");
  }
});

$(".list-lang li").click(function () {
  const lang = $(this).data("lang");
  $(".lg-choose").html(lang);
  $("#langCode").val(lang);
  setTimeout(translate(), 1000);
});

function translate() {
  var langCode = $("#langCode").val() || "en";
  $.getJSON("../lang/" + langCode + ".json", function (data) {
    $("[langKey]").each(function (index) {
      var strTr = data[$(this).attr("langKey")];
      $(this).html(strTr);
    });
  }).fail(function () {
    console.log("An error has occurred.");
  });
}

$(document).click(function (event) {
  if (!$(event.target).closest(".language").length) {
    $(".language .list-lang").removeClass("active");
  }
});

$("header .list-page li:not(.launch-mb)").click(function () {
  const classes = String($(this).attr("class"));
  const listClasses = classes.split(" ");

  // inactive
  if (!$(this).hasClass("to")) {
    $(".to").addClass("from").removeClass("to");
    $(this).addClass("to");
  }

  // active
  if (!$(this).hasClass("activePage")) {
    $("header .list-page li").removeClass("activePage");
    $(this).addClass("activePage");
  }

  // scoll to id
  if (listClasses.includes("homepage")) {
    $("html, body").animate({ scrollTop: "0px" });
    return;
  }
  if (listClasses.includes("social-media")) {
    scollByLi("#social-media", 80);
    return;
  }
  if (listClasses.includes("about-us")) {
    scollByLi("#about-us", 100);
    return;
  }
  if (listClasses.includes("news")) {
    scollByLi("#news", 20);
    return;
  }
  if (listClasses.includes("games")) {
    scollByLi("#our-games", 60);
    return;
  }
  if (listClasses.includes("contact-us")) {
    scollByLi("#contact-us", 0);
    return;
  }
  if (listClasses.includes("career")) {
    $("header .list-page li").removeClass("from");
  }
});

function scollByLi(id, w1400) {
  let width = $(document).width();
  let top = $(id).offset().top;
  if (width > 1200) {
    top = top - w1400;
  }
  $("html, body").animate({ scrollTop: top + "px" }, 200);
}

$(".logo-mind_ventures").click(function () {
  const prefix = "header .list-page";
  if (!$(prefix + " .homepage").hasClass("to")) {
    $(".to").addClass("from").removeClass("to");
    $(prefix + " .homepage").addClass("to");
    $("html, body").animate({ scrollTop: "0px" });
  }
});

$(window).scroll(function () {
  active();
});

function active() {
  let height_banner = $("#banner").height();
  let heigth_social_media = $("#social-media").height();
  let height_about_us = $("#about-us").height();
  let height_news = $("#news").height();
  let height_our_games = $("#our-games").height();
  let height_contact_us = $("#contact-us").height();

  let distance_banner = $("#banner").offset().top;
  let distance_social_media = $("#social-media").offset().top;
  let distance_about_us = $("#about-us").offset().top;
  let distance_news = $("#news").offset().top;
  let distance_our_games = $("#our-games").offset().top;
  let distance_contact_us = $("#contact-us").offset().top;
  let scrollY = window.pageYOffset;

  if (scrollY >= 0 && scrollY <= (distance_banner + height_banner) / 2) {
    addTo(" .homepage");
    return;
  }
  if (
    scrollY > (distance_banner + height_banner) / 2 &&
    scrollY <= (distance_social_media + heigth_social_media) / 2 + 230
  ) {
    addTo(" .social-media");
    return;
  }
  if (
    scrollY > (distance_social_media + heigth_social_media) / 2 + 230 &&
    scrollY <= (distance_about_us + height_about_us) / 2 + 600
  ) {
    addTo(" .about-us");
    return;
  }
  if (
    scrollY > (distance_about_us + height_about_us) / 2 + 600 &&
    scrollY <= distance_news + height_news - 300
  ) {
    addTo(" .news");
    return;
  }
  if (
    scrollY > distance_news + height_news - 300 &&
    scrollY <= distance_our_games + height_our_games - 200
  ) {
    addTo(" .games");
    return;
  }
  if (scrollY > distance_our_games + height_our_games - 200) {
    addTo(" .contact-us");
    return;
  }
}

function addTo(name) {
  const prefix = "header .list-page";
  let to = $(prefix + " li.to").index();
  let from = $(prefix + " li.from").index();
  if (from === -1) {
    $(prefix + " li").removeClass("activePage");
    $(prefix + name).addClass("activePage");
    $(prefix + " li").removeClass("to");
    $(prefix + name).addClass("to");
  } else {
    if (Math.abs(to - from) > 0) {
      if ($(prefix + name).hasClass("to")) {
        $(prefix + " li").removeClass("from");
      }
    }
  }
}

$(".our--list").slick({
  dots: true,
  infinite: true,
  variableWidth: true,
  prevArrow:
    "<img class='slick-arrow slick-prev' src='./images/left.svg'>",
  nextArrow:
    "<img class='slick-arrow slick-next' src='./images/right.svg'>",
});
